package crowdperf;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import com.atlassian.crowd.integration.Constants;
import com.atlassian.crowd.integration.rest.service.RestCrowdClient;
import com.atlassian.crowd.service.client.ClientPropertiesImpl;
import com.atlassian.crowd.service.client.CrowdClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;
import com.atlassian.crowd.service.soap.client.SecurityServerClientImpl;
import com.atlassian.crowd.service.soap.client.SoapClientPropertiesImpl;

public class CrowdPerfUtil
{
    static Properties loadProperties() throws IOException
    {
        Properties props = new Properties();
        InputStream in = new FileInputStream("crowd.properties");
        try
        {
            props.load(in);
        }
        finally
        {
            in.close();
        }

        return props;
    }

    public static CrowdClient newCrowdClient() throws IOException
    {
        return new RestCrowdClient(ClientPropertiesImpl.newInstanceFromProperties(loadProperties()));
    }

    public static SecurityServerClient newSecurityServerClient() throws IOException
    {
        return new SecurityServerClientImpl(SoapClientPropertiesImpl.newInstanceFromProperties(loadProperties()));
    }

    public static String getBaseUrl() throws IOException
    {
        return loadProperties().getProperty(Constants.PROPERTIES_FILE_SECURITY_SERVER_URL).replaceAll("/services$", "");
    }

    public static String makeUsername(int i) {return "user" + i;}
    public static String makePassword(String username) {return username ;}
}
