package crowdperf.setup;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.GroupNotFoundException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidCredentialException;
import com.atlassian.crowd.exception.InvalidGroupException;
import com.atlassian.crowd.exception.InvalidUserException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.integration.rest.entity.GroupEntity;
import com.atlassian.crowd.model.group.GroupType;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.service.client.CrowdClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import crowdperf.CrowdPerfUtil;
import crowdperf.behaviour.ApplicationSingleUserActor;
import crowdperf.behaviour.GetCookieConfigActor;

import static crowdperf.CrowdPerfUtil.makePassword;
import static crowdperf.CrowdPerfUtil.makeUsername;

/**
 * Simulate a small load on a Crowd server using REST endpoints
 */
public class LoadSimulator
{
    public static final String DEFAULT_USERNUM = "10";
    private static final Logger log = LoggerFactory.getLogger(LoadSimulator.class);
    public static final String DEFAULT_THREAD_COUNT = "10";
    public static final String USERS = "users";
    public static final String LICENSED_CONFLUENCE = "_licensed_confluence";
    private static CrowdClient client;

    private static CountDownLatch latch;
    private static int userCount;
    private static int threadCount;

    public static void main(String[] args) throws Exception
    {
        parseParams();

        log.info("=============== START OF SIMULATION : {} USERS, {} THREADS ============", userCount, threadCount);

        client = CrowdPerfUtil.newCrowdClient();

        setupGroups();
        setupUsers(userCount);

        final ExecutorService executorService = Executors.newFixedThreadPool(threadCount);

        latch = new CountDownLatch(userCount);

        // submit cookie getter thread
        executorService.submit(new GetCookieConfigActor());

        // submit fake users thread
        for (int i = 0; i < userCount; i++)
        {
            executorService.submit(new ApplicationSingleUserActor(makeUsername(i), latch));
        }

        latch.await(3, TimeUnit.MINUTES);
        executorService.shutdownNow();
        log.info("=============== END OF SIMULATION ============");
    }

    private static void setupGroups()
            throws ApplicationPermissionException, GroupNotFoundException, OperationFailedException, InvalidAuthenticationException, InvalidGroupException
    {
        try
        {
            client.getGroup(LICENSED_CONFLUENCE);
        }
        catch (GroupNotFoundException e)
        {
            log.info("Creating _licensed_group");
            client.addGroup(new GroupEntity(LICENSED_CONFLUENCE, "users who can access Confluence", GroupType.GROUP, true));
        }
    }

    private static void parseParams()
    {
        final String userCountAsString = System.getProperty("userCount", DEFAULT_USERNUM);
        final String threadCountAsString = System.getProperty("threadCount", DEFAULT_THREAD_COUNT);
        userCount = Integer.valueOf(userCountAsString);
        threadCount = Integer.valueOf(threadCountAsString);
    }

    private static void setupUsers(int userNum)
            throws InvalidUserException, InvalidCredentialException, OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, GroupNotFoundException, UserNotFoundException
    {
        for (int i = 0; i < userNum; i++)
        {
            final String username = makeUsername(i);
            tryRemoveUser(username);
            createUser(username, makePassword(username));
            addUserToGroups(username);
        }
    }

    private static void addUserToGroups(String username)
            throws GroupNotFoundException, UserNotFoundException, OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException
    {
        client.addUserToGroup(username, USERS);
        client.addUserToGroup(username, LICENSED_CONFLUENCE);
    }

    private static void createUser(String username, String password)
            throws InvalidUserException, InvalidCredentialException, OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException
    {
        UserTemplate arg0 = new UserTemplate(username);
        arg0.setActive(true);
        log.info("Creating user {}", username);
        client.addUser(arg0, new PasswordCredential(password));
    }

    private static void tryRemoveUser(String username)
    {
        try
        {
            log.info("Removing user: {}", username);
            client.removeUser(username);
        }
        catch (Exception e)
        {
            log.warn("Could not remove user {}", username);
        }
    }
}
