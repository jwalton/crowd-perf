package crowdperf.setup;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.user.UserTemplate;
import com.atlassian.crowd.service.client.CrowdClient;

import crowdperf.CrowdPerfUtil;

public class CreateUsers
{
    public static void main(String[] args) throws Exception
    {
        CrowdClient client = CrowdPerfUtil.newCrowdClient();

        for (int i = 0; i < 20; i++)
        {
//            client.removeUser("user" + i);

            UserTemplate arg0 = new UserTemplate("user" + i);
            arg0.setActive(true);

            client.addUser(arg0, new PasswordCredential("user"));
        }
    }
}
