package crowdperf.setup;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import com.atlassian.crowd.search.query.entity.restriction.NullRestrictionImpl;
import com.atlassian.crowd.service.client.CrowdClient;

import crowdperf.CrowdPerfUtil;

/**
 * Adds all users to a single group, for setting up groups with large numbers
 * of members.
 */
public class AddAllUsersToGroup
{
    static String GROUP_NAME = "group/with/slashes";

    public static void main(String[] args) throws Exception
    {
        CrowdClient client = CrowdPerfUtil.newCrowdClient();

        List<String> users = client.searchUserNames(NullRestrictionImpl.INSTANCE, 0, -1);

        System.out.println("Total users: " + users.size());

        Set<String> alreadymembers = new HashSet<String>(client.getNamesOfUsersOfGroup(GROUP_NAME, 0, -1));
        System.out.println("Existing members: " + alreadymembers.size());

        List<String> toAdd = new ArrayList<String>(users);
        toAdd.removeAll(alreadymembers);

        System.out.println("To add: " + toAdd.size());

        int added = 0;

        for (String username : toAdd)
        {
            client.addUserToGroup(username, GROUP_NAME);

            added++;

            System.out.println(added + "/" + toAdd.size());
        }
    }
}
