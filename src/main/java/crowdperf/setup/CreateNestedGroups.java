package crowdperf.setup;

import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.group.GroupTemplate;
import com.atlassian.crowd.service.client.CrowdClient;

import crowdperf.CrowdPerfUtil;

/**
 * In a directory where nested groups are permitted, create groups with sub-groups.
 */
public class CreateNestedGroups
{
    public static void main(String[] args) throws Exception
    {
        CrowdClient client = CrowdPerfUtil.newCrowdClient();

        int GROUPS = 1000;
        int SUBGROUPS = 10;

        for (int i = 0; i < GROUPS; i++)
        {
            Group group = new GroupTemplate("group-" + i);

            client.addGroup(group);
//            client.removeGroup(group.getName());

            for (int j = 0; j < SUBGROUPS; j++)
            {
                Group subgroup = new GroupTemplate("subgroup-" + i + "-" + j);
                client.addGroup(subgroup);
                client.addGroupToGroup(subgroup.getName(), group.getName());
//                client.removeGroup(subgroup.getName());
            }

            System.out.println( (100.0 * i) / GROUPS);
        }
    }
}
