package crowdperf.bench;

import java.io.IOException;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpException;
import org.apache.commons.httpclient.NameValuePair;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;

import crowdperf.CrowdPerfUtil;

/**
 * Log in, then repeatedly load the Crowd console.
 */
public class FetchCrowdConsole
{
    private final String baseUrl;
    private final HttpClient httpClient;

    public FetchCrowdConsole(String baseUrl, HttpClient httpClient)
    {
        this.baseUrl = baseUrl;
        this.httpClient = httpClient;
    }

    void fetchConsoleOrFail() throws HttpException, IOException
    {
        GetMethod gm = new GetMethod(baseUrl + "/console/secure/console.action");
        gm.setFollowRedirects(false);

        int rc = httpClient.executeMethod(gm);
        if (rc != 200)
        {
            throw new IOException("Unexpected status for console: " + gm.getStatusLine());
        }
    }

    void performLogin() throws HttpException, IOException
    {
        PostMethod pm = new PostMethod(baseUrl + "/console/j_security_check");
        NameValuePair[] data = {
                new NameValuePair("j_username", "admin"),
                new NameValuePair("j_password", "admin")
        };
        pm.setRequestBody(data);

        if (httpClient.executeMethod(pm) != 302)
        {
            throw new IOException("Unexpected status for login attempt: " + pm.getStatusLine());
        }
    }

    public static void main(String[] args) throws HttpException, IOException
    {
        FetchCrowdConsole fcc = new FetchCrowdConsole(
                CrowdPerfUtil.getBaseUrl(),
                new HttpClient());

        // This will fail when unauthenticated
//        fcc.fetchConsoleOrFail();

        fcc.performLogin();

        System.out.println("stamp,count");

        long count = 0;

        while(true)
        {
            System.out.println(System.currentTimeMillis() + "," + count);
            fcc.fetchConsoleOrFail();

            count++;
        }
    }
}
