package crowdperf.bench;

import com.atlassian.crowd.integration.soap.SOAPNestableGroup;
import com.atlassian.crowd.service.soap.client.SecurityServerClient;

import crowdperf.CrowdPerfUtil;

/**
 * How long does {@link SecurityServerClient#findAllGroupRelationships()} take?
 */
public class BenchSoapAllMemberships
{
    public static void main(String[] args) throws Exception
    {
        SecurityServerClient client = CrowdPerfUtil.newSecurityServerClient();

        for (int i = 0; i < 10; i++)
        {
            long start = System.currentTimeMillis();

            SOAPNestableGroup[] rels = client.findAllGroupRelationships();

            long duration = System.currentTimeMillis() - start;

            System.out.println(rels.length + "," + (duration / 1000.0));
        }
    }
}
