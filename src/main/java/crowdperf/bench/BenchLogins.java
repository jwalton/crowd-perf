package crowdperf.bench;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.user.User;
import com.atlassian.crowd.service.client.CrowdClient;

import crowdperf.CrowdPerfUtil;

public class BenchLogins
{
    public static void main(String[] args) throws Exception
    {
        Collection<String> results = new ArrayList<String>();

        final int TOTAL = 1000;

        for (int j = 0; j < 2; j++)
        {
            for (int i = 1; i < 20; i+=2)
            {
                final int T = i;
                final int N = TOTAL / T;

                results.add(testWithThreads(T, N));
            }
        }

        System.out.println("----");
        for (String s : results)
        {
            System.out.println(s);
        }
    }

    /**
     * A basic cycle of login, get details, validate session, logout.
     */
    static String testWithThreads(final int T, final int N) throws Exception
    {
        long start = System.currentTimeMillis();

        Collection<Thread> threads = new ArrayList<Thread>();

        for (int t = 0; t < T; t++)
        {
            final String username = "user" + t;
//            final String username = "alias" + t;

            Thread thr = new Thread() {
                public void run()
                {
                    try
                    {
                        CrowdClient client = CrowdPerfUtil.newCrowdClient();

                        client.testConnection();

                        for (int i = 0; i < N; i++)
                        {
                            PasswordCredential pc = new PasswordCredential("user");

                            ValidationFactor[] vf = {
                                    new ValidationFactor(ValidationFactor.REMOTE_ADDRESS, "999.999.999.999")
                            };

                            UserAuthenticationContext arg0 = new UserAuthenticationContext(username, pc, vf, "crowd");

                            String token = client.authenticateSSOUser(arg0);

                            System.out.println(token);

                            User user = client.findUserFromSSOToken(token);

                            System.out.println(user);

                            client.validateSSOAuthentication(token, Arrays.asList(vf));

                            client.invalidateSSOToken(token);
                            System.out.println(i);
                        }
                    }
                    catch (Exception e)
                    {
                        e.printStackTrace();
                    }
                }
            };

            threads.add(thr);
            thr.start();
        }

        for (Thread th : threads)
        {
            th.join();
        }

        long duration = System.currentTimeMillis() - start;

        return N + "," + T + "," + duration;
    }
}
