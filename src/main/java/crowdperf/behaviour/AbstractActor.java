package crowdperf.behaviour;


import java.io.IOException;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.service.client.CrowdClient;

import crowdperf.CrowdPerfUtil;

public abstract class AbstractActor implements Runnable
{
    protected CrowdClient client;

    public AbstractActor()
            throws IOException, OperationFailedException, ApplicationPermissionException, InvalidAuthenticationException
    {
        this.client = CrowdPerfUtil.newCrowdClient();
        client.testConnection();
    }

}
