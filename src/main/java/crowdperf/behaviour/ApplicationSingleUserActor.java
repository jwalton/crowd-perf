package crowdperf.behaviour;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import com.atlassian.crowd.embedded.api.PasswordCredential;
import com.atlassian.crowd.exception.ApplicationAccessDeniedException;
import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.ExpiredCredentialException;
import com.atlassian.crowd.exception.InactiveAccountException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.InvalidTokenException;
import com.atlassian.crowd.exception.OperationFailedException;
import com.atlassian.crowd.exception.UserNotFoundException;
import com.atlassian.crowd.model.authentication.UserAuthenticationContext;
import com.atlassian.crowd.model.authentication.ValidationFactor;
import com.atlassian.crowd.model.group.Group;
import com.atlassian.crowd.model.user.User;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static crowdperf.CrowdPerfUtil.makePassword;

/**
 * Simulate the REST calls made by an application during the lifetime of an user.
 * <ol>
 *     <li>login</li>
 *     <li>find User from SSO token</li>
 *     <li>validate session (5 times)</li>
 *     <li>logout</li>
 * </ol>
 */
public class ApplicationSingleUserActor extends AbstractActor
{
    private static final Logger log = LoggerFactory.getLogger(ApplicationSingleUserActor.class);

    public static final int MS_BETWEEN_AUTHENTICATION_CHECKS = 1000;
    private final String password;
    private final CountDownLatch latch;

    String username;

    public ApplicationSingleUserActor(String username, CountDownLatch latch)
            throws ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException, IOException
    {
        super();
        this.username = username;
        password = makePassword(username);
        this.latch = latch;
    }


    @Override
    public void run()
    {
        try
        {
            log.info("Start of simulation for user {}", username);
            String token = login();
            findUserFromSSOToken(token);
            checkSessionNTimes(token, 5);
            logout(token);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        finally
        {
            log.info("End of simulation for user {}", username);
            latch.countDown();
        }
    }

    private void logout(String token)
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException
    {
        log.info("Invalidating token for user {}", username);
        client.invalidateSSOToken(token);
    }

    private void findUserFromSSOToken(String token)
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, InvalidTokenException
    {
        User user = client.findUserFromSSOToken(token);
        log.info("Found user {} for token {}", user.getName(), token);
    }

    private String login()
            throws ApplicationAccessDeniedException, InactiveAccountException, ExpiredCredentialException, ApplicationPermissionException, InvalidAuthenticationException, OperationFailedException
    {
        PasswordCredential pc = new PasswordCredential(password);
        ValidationFactor[] vf = new ValidationFactor[0];

        final String application = "confluence";
        UserAuthenticationContext userAuthenticationContext = new UserAuthenticationContext(username, pc, vf, application);
        log.info("Authenticating user {} from application {}", username, application);
        return client.authenticateSSOUser(userAuthenticationContext);
    }

    private void checkSessionNTimes(String token, int numberOfChecks)
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, InvalidTokenException, InterruptedException, UserNotFoundException, InactiveAccountException, ExpiredCredentialException
    {
        for (int i = 0; i < numberOfChecks; i++)
        {
            checkSSOAuthentication(token);
            getGroupsForUser();
            Thread.sleep(MS_BETWEEN_AUTHENTICATION_CHECKS);
        }
    }

    private void getGroupsForUser()
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, UserNotFoundException
    {
        log.info("Getting groups for user {}", username);
        final List<Group> groups = client.getGroupsForNestedUser(username, 0, 10);
        log.info("Found {} groups for user {}", groups.size(), username);
    }

    private void checkSSOAuthentication(String token)
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, InvalidTokenException
    {
        log.info("Validating SSO for token {}", token);
        client.validateSSOAuthentication(token, Collections.<ValidationFactor>emptyList());
    }
}
