package crowdperf.behaviour;

import java.io.IOException;

import com.atlassian.crowd.exception.ApplicationPermissionException;
import com.atlassian.crowd.exception.InvalidAuthenticationException;
import com.atlassian.crowd.exception.OperationFailedException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class GetCookieConfigActor extends AbstractActor
{
    private static final Logger log = LoggerFactory.getLogger(GetCookieConfigActor.class);

    public static final int MS_BETWEEN_CHECKS = 500;

    public GetCookieConfigActor()
            throws IOException, OperationFailedException, ApplicationPermissionException, InvalidAuthenticationException
    {
        super();
    }

    @Override
    public void run()
    {
        try
        {
            getCookieConfigPeriodically();
        }
        catch (InterruptedException ie)
        {
            log.debug("Interrupted");
        }
        catch (Exception e)
        {
            log.error("Could not get cookie configuration", e);
        }
    }

    private void getCookieConfigPeriodically()
            throws OperationFailedException, InvalidAuthenticationException, ApplicationPermissionException, InterruptedException
    {
        while (true)
        {
            log.info("Getting cookie configuration");
            client.getCookieConfiguration();
            Thread.sleep(MS_BETWEEN_CHECKS);
        }
    }

}
